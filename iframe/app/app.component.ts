import { Component } from '@angular/core';

@Component({
    selector: 'my-app',
    template: `<h1>ListView with Item template demo</h1>
    <div *ngFor="let item of items">
      <h3>{{item.name}}</h3>
      <h4>{{item.description}}</h4>
    </div>

`
})
export class AppComponent {
    private items = [{
        name: 'Blup',
        description: 'Nemo says blup'
    },{
        name: 'FooBar',
        description: 'foobar is awesome'
    }]
}